import React, { Component } from "react";
import { selectors } from "../../redux";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import flow from "lodash.flow";

import "bootstrap/dist/css/bootstrap.css";
class Display extends Component {
  cleanEquation(){
    const {equation} = this.props;
    let cleanEquation = equation.trim().replace("*", "×");
    if(equation.length > 1){
      cleanEquation =cleanEquation.replace(/^0+/, '')
    }
    return cleanEquation
  }
  render() {
    return (
      <div>
        <div className="display_current_equation">
          {this.cleanEquation()}
        </div>
        <div>
          <input
            type="text"
            placeholder={this.props.displayText}
            disabled
          ></input>
        </div>
      </div>
    );
  }
}

Display.propTypes = {
  equation: PropTypes.string,
};
export const mapStateToProps = (state) => ({
  equation: selectors.getEquation(state),
  displayText: selectors.getDisplayValue(state),
});

const enhance = flow(connect(mapStateToProps));
export default enhance(Display);
