export const getEquation = (state) => state.equation.currentEquation;
export const getDisplayValue = (state) => state.equation.displayValue;
