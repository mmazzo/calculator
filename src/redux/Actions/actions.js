export const setPercent = () => ({
  type: "PERCENT",
});
export const setSign = () => ({
  type: "SIGN",
});
export const setEquation = (equation) => ({
  type: "EQUATION",
  equation,
});
